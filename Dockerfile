FROM openjdk:15-ea-11-jdk-slim

COPY target/Tecnica-0.0.1-SNAPSHOT.jar /usr/share/app.jar

CMD ["java", "-jar", "/usr/share/app.jar"]